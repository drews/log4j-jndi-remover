package utils

import (
	"errors"
	"io/fs"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"

	"github.com/apex/log"
	"github.com/iafan/cwalk"
)

func CollectTargets(parentPath string) ([]string, error) {
	log.Debug("Collecting targets")
	parentPath = SanitizePath(parentPath)
	log.Debugf("Sanitized path: %s", parentPath)
	if _, err := os.Stat(parentPath); errors.Is(err, os.ErrNotExist) {
		return nil, errors.New("target does not exist")
	}

	var isDir bool

	log.Debug("Determining if target is a directory")
	if runtime.GOOS == "windows" {
		fi, err := os.Stat(parentPath)
		if err != nil {
			log.WithFields(log.Fields{
				"path": parentPath,
			}).WithError(err).Warn("Could not stat path, so assuming this is a dir, thanks windoz")
			isDir = true
		} else {
			isDir = fi.IsDir()
		}
	} else {
		fi, err := os.Stat(parentPath)
		if err != nil {
			log.WithFields(log.Fields{
				"path": parentPath,
			}).WithError(err).Warn("Could not stat path")
			return nil, err
		}
		isDir = fi.Mode().IsDir()
	}
	if isDir {
		log.WithFields(log.Fields{
			"parent_path": parentPath,
		}).Infof("⏳ Collecting jar files from directory")

		var walkmatch func(string, string) ([]string, error)
		if runtime.GOOS == "windows" {
			log.Debug("Using windows walkmatch")
			walkmatch = WalkMatchWindows
		} else {
			log.Debug("Using generic walkmatch")
			walkmatch = WalkMatch
		}

		targets, err := walkmatch(parentPath, "*.jar")
		if err != nil {
			log.WithFields(log.Fields{
				"path": parentPath,
			}).WithError(err).Warn("Could not check dir")
		}
		return targets, nil
	} else {
		log.Debugf("Yeah, I thinkg %v is a file", parentPath)
		return []string{parentPath}, nil
	}
}

func WalkMatch(root, pattern string) ([]string, error) {
	var matches []string
	// Parallel walk with cwalk module
	err := cwalk.Walk(root, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			return nil
		}
		if matched, err := filepath.Match(pattern, filepath.Base(path)); err != nil {
			log.WithFields(log.Fields{
				"path": root,
			}).WithError(err).Warn("Could not check dir")
		} else if matched {
			matches = append(matches, filepath.Join(root, path))
		}
		return nil
	})
	if err != nil {
		// return nil, err
		for _, errors := range err.(cwalk.WalkerErrorList).ErrorList {
			log.WithError(errors).Warn("🤕 Encountered error while walking tree")
		}
	}
	return matches, nil
}

// Windows requires some more love & care regarding filewalks
func WalkMatchWindows(root, pattern string) ([]string, error) {
	var matches []string
	// Parallel walk with cwalk module
	matches = FindByExt(root, ".jar")
	return matches, nil
}

func HasZip() bool {
	_, err := exec.LookPath("zip")
	if err != nil {
		return false
	}
	_, err = exec.LookPath("unzip")
	if err != nil {
		return false
	}

	return true
}

func SanitizePath(fpath string) string {
	fpath = strings.TrimSuffix(fpath, "\\")
	fpath = filepath.Clean(fpath)
	if runtime.GOOS == "windows" {
		// Trailing backslashes make it weird
		fpath = strings.ReplaceAll(fpath, string(filepath.Separator), "/")
		return fpath
	} else {
		return fpath
	}
}

func FindByExt(root, ext string) []string {
	var a []string
	filepath.WalkDir(root, func(s string, d fs.DirEntry, e error) error {
		if e != nil {
			log.WithFields(log.Fields{
				"path": s,
			}).WithError(e).Warn("🤕 Encountered error while walking tree")
			return nil
		}
		if filepath.Ext(d.Name()) == ext {
			a = append(a, s)
		}
		return nil
	})
	return a
}
