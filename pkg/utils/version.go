package utils

import hversion "github.com/hashicorp/go-version"

func IsVersionVulnerable(currentV, minV, maxV *hversion.Version) bool {
	if currentV.LessThanOrEqual(maxV) && currentV.GreaterThanOrEqual(minV) {
		return true
	}
	return false
}
