package utils

import (
	"errors"
	"fmt"
	"io"
	"os"
	"path"
	"time"

	"github.com/apex/log"
	"github.com/mitchellh/go-homedir"
)

func BackupFile(target string) (string, error) {
	if _, err := os.Stat(target); errors.Is(err, os.ErrNotExist) {
		return "", errors.New("target file does not exist")
	}

	backupDir, err := homedir.Expand("~/.log4shell-fix-backups")
	if err != nil {
		return "", err
	}
	if _, err := os.Stat(backupDir); errors.Is(err, os.ErrNotExist) {
		log.WithFields(log.Fields{
			"backup_dir": backupDir,
		}).Infof("Creating backup directory")
		err := os.MkdirAll(backupDir, os.ModePerm)
		if err != nil {
			return "", err
		}
	}

	backup := path.Join(backupDir, fmt.Sprintf("%s-%s", path.Base(target), time.Now().Format("2006-01-02-15-04-05.000000000")))
	_, err = copy(target, backup)
	if err != nil {
		return "", err
	}

	return backup, nil
}

func copy(src, dst string) (int64, error) {
	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return 0, err
	}

	if !sourceFileStat.Mode().IsRegular() {
		return 0, fmt.Errorf("%s is not a regular file", src)
	}

	source, err := os.Open(src)
	if err != nil {
		return 0, err
	}
	defer source.Close()

	destination, err := os.Create(dst)
	if err != nil {
		return 0, err
	}
	defer destination.Close()
	nBytes, err := io.Copy(destination, source)
	return nBytes, err
}
