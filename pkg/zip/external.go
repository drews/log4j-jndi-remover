package zip

import (
	"archive/zip"
	"errors"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"

	"github.com/apex/log"
	hversion "github.com/hashicorp/go-version"

	"gitlab.oit.duke.edu/drews/log4j-jndi-remover/pkg/utils"
)

type ExternalZipScanner struct{}

func (e *ExternalZipScanner) Scan(jar string) (bool, error) {
	if _, err := os.Stat(jar); errors.Is(err, os.ErrNotExist) {
		return false, errors.New("jar file does not exist")
	}

	contents, err := exec.Command("unzip", "-l", jar).Output()
	if err != nil {
		return false, err
	}

	out := string(contents)
	for _, line := range strings.Split(out, "\n") {
		if strings.HasSuffix(line, jndiClass) {
			return true, nil
		}
	}

	return false, nil
}

func (e *ExternalZipScanner) ScanVersion(jar string) (*hversion.Version, error) {
	if _, err := os.Stat(jar); errors.Is(err, os.ErrNotExist) {
		return nil, errors.New("jar file does not exist")
	}
	r, err := zip.OpenReader(jar)
	if err != nil {
		return nil, err
	}
	defer r.Close()

	for _, f := range r.File {
		if strings.HasSuffix(f.Name, "pom.properties") && strings.Contains(f.Name, "org.apache.logging.log4j") {
			reader, err := f.Open()
			if err != nil {
				return nil, err
			}
			defer reader.Close()
			b, err := ioutil.ReadAll(reader)
			if err != nil {
				return nil, err
			}
			for _, line := range strings.Split(string(b), "\n") {
				if strings.HasPrefix(line, "version=") {
					versionS := strings.Split(line, "=")[1]
					v, err := hversion.NewVersion(versionS)
					if err != nil {
						return nil, err
					}
					return v, nil
				}
			}

			return nil, errors.New("No Version found")
		}
	}
	return nil, errors.New("No version found")
}

func (e *ExternalZipScanner) Fix(jar string, backup bool) error {
	if _, err := os.Stat(jar); errors.Is(err, os.ErrNotExist) {
		return errors.New("jar file does not exist")
	}
	var err error

	if backup {
		newFile, err := utils.BackupFile(jar)
		if err != nil {
			log.WithFields(log.Fields{
				"backup": newFile,
				"target": jar,
			}).WithError(err).Warn("💾 Backup NOT created")
			return err
		} else {
			log.WithFields(log.Fields{
				"backup": newFile,
				"target": jar,
			}).Info("💾 Backup created")
		}
	} else {
		log.WithFields(log.Fields{
			"target": jar,
			"backup": backup,
		}).Info("⛔️ Skipping backup")
	}

	// Note, this zip command always exists non-zero, so we can ignore the error
	err = exec.Command("zip", "-d", jar, jndiClass).Run()
	if err != nil {
		return err
	}

	return nil
}

func NewExternalZipScanner() *ExternalZipScanner {
	return &ExternalZipScanner{}
}
