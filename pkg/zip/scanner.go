package zip

import (
	"github.com/apex/log"
	hversion "github.com/hashicorp/go-version"
	"gitlab.oit.duke.edu/drews/log4j-jndi-remover/pkg/utils"
)

var jndiClass string = "org/apache/logging/log4j/core/lookup/JndiLookup.class"

type Scanner interface {
	Scan(jar string) (bool, error)
	ScanVersion(jar string) (*hversion.Version, error)
	Fix(jar string, backup bool) error
}

func NewScanner(kind string) Scanner {
	// type scanner struct{}
	var scanner Scanner
	switch kind {
	case "internal":
		scanner = NewInternalZipScanner()
		log.WithFields(log.Fields{
			"zip-scanner": "internal",
		}).Info("🤐 Selecting internal zip scanner")
	case "external":
		scanner = NewExternalZipScanner()
		log.WithFields(log.Fields{
			"zip-scanner": "external",
		}).Info("🤐 Selecting external zip scanner")
	case "auto":
		if utils.HasZip() {
			scanner = NewExternalZipScanner()
			log.WithFields(log.Fields{
				"zip-scanner": "external",
			}).Info("🤐 Auto-Selecting external zip scanner")
		} else {
			scanner = NewInternalZipScanner()
			log.WithFields(log.Fields{
				"zip-scanner": "internal",
			}).Info("🤐 Auto-Selecting internal zip scanner")
		}
	default:
		panic("Invalid option")
	}
	return scanner
}
