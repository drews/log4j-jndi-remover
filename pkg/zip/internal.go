package zip

import (
	"archive/zip"
	"errors"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	hversion "github.com/hashicorp/go-version"

	"github.com/apex/log"

	"gitlab.oit.duke.edu/drews/log4j-jndi-remover/pkg/utils"
)

type InternalZipScanner struct{}

func (s *InternalZipScanner) Scan(jar string) (bool, error) {
	if _, err := os.Stat(jar); errors.Is(err, os.ErrNotExist) {
		return false, errors.New("jar file does not exist")
	}
	r, err := zip.OpenReader(jar)
	if err != nil {
		return false, err
	}
	defer r.Close()

	for _, f := range r.File {
		if strings.HasSuffix(f.Name, jndiClass) {
			return true, nil
		}
	}
	return false, nil
}

func (s *InternalZipScanner) ScanVersion(jar string) (*hversion.Version, error) {
	if _, err := os.Stat(jar); errors.Is(err, os.ErrNotExist) {
		return nil, errors.New("jar file does not exist")
	}
	r, err := zip.OpenReader(jar)
	if err != nil {
		return nil, err
	}
	defer r.Close()

	for _, f := range r.File {
		if strings.HasSuffix(f.Name, "pom.properties") && strings.Contains(f.Name, "org.apache.logging.log4j") {
			reader, err := f.Open()
			if err != nil {
				return nil, err
			}
			defer reader.Close()
			b, err := ioutil.ReadAll(reader)
			if err != nil {
				return nil, err
			}
			for _, line := range strings.Split(string(b), "\n") {
				if strings.HasPrefix(line, "version=") {
					versionS := strings.Split(line, "=")[1]
					v, err := hversion.NewVersion(versionS)
					if err != nil {
						return nil, err
					}
					return v, nil
				}
			}
			return nil, errors.New("No Version found")
		}
	}
	return nil, errors.New("No version found")
}

func (s *InternalZipScanner) Fix(jar string, backup bool) error {
	if _, err := os.Stat(jar); errors.Is(err, os.ErrNotExist) {
		return errors.New("jar file does not exist")
	}
	var err error

	if backup {
		newFile, err := utils.BackupFile(jar)
		if err != nil {
			log.WithFields(log.Fields{
				"backup": newFile,
				"target": jar,
			}).WithError(err).Warn("💾 Backup NOT created")
			return err
		} else {
			log.WithFields(log.Fields{
				"backup": newFile,
				"target": jar,
			}).Info("💾 Backup created")
		}
	} else {
		log.WithFields(log.Fields{
			"target": jar,
			"backup": backup,
		}).Info("⛔️ Skipping backup")
	}

	tmpdir, err := ioutil.TempDir("", "log4j-remover-tmpdir")
	defer os.RemoveAll(tmpdir)
	if err != nil {
		return err
	}
	newZip := filepath.Join(tmpdir, filepath.Base(jar))
	log.Infof("%+v", newZip)
	jarR, err := zip.OpenReader(jar)
	if err != nil {
		return err
	}
	defer jarR.Close()

	// Prepare new zip
	archive, err := os.Create(newZip)
	if err != nil {
		return err
	}

	newZipW := zip.NewWriter(archive)
	defer newZipW.Close()

	// Cycle through original zip, and copy files in
	for _, f := range jarR.File {
		if strings.HasSuffix(f.Name, jndiClass) {
			continue
		}
		w1, err := newZipW.Create(f.Name)
		if err != nil {
			return err
		}
		fc, err := f.Open()
		if err != nil {
			return err
		}
		if _, err := io.Copy(w1, fc); err != nil {
			return err
		}
	}

	// Finally, rename
	err = os.Rename(newZip, jar)
	if err != nil {
		return err
	}

	return nil
}

func NewInternalZipScanner() *InternalZipScanner {
	return &InternalZipScanner{}
}
