# Log4J JNDI Remover

Scans for isntances of the Jndi Lookup class in `.jar` files, and removes them if told

## What does this actually do?

This tool looks at the contents of a jar file (or set of jar files), and
determines if it contains the
`org/apache/logging/log4j/core/lookup/JndiLookup.class` file. When told to
`fix`, the JndiLookup.class file is removed from the .jar

Before modifyng the jar, a backup is copied to `~/.log4shell-fix-backups` in the
event a restore is needed

If `zip` and `unzip` commands are in your `$PATH`, they will be used to modify
the jar. If these commands are not present, the app will use the standard go zip
library to do the zip manipulation, which may be less effeciant. You can force
either style with the `--zip-scanner` argument

## Usage

`scan` subcommand scans a given file or subdirectory for the existance of the lookup

`fix` subcommand makes a backup of vulnerable files, and then removes the lookup from the jar

### Example

```bash
root@drews-dev-02 [development] ~ $ /tmp/log4j-jndi-remover scan /srv/some-app
   • ⏳ Collecting jar files from directory parent_path=/srv/some-app
   • 🎯 Found possible targets  found=2 parent=/srv/some-app
   • 🤩 No JNDI class path found target=/srv/some-app/jruby_cache_backend.jar vulnerable=false
   • 🤢 Found JNDI class path   target=/srv/some-app/logstash-input-tcp-6.2.3.jar vulnerable=true
   • 🤙 Finished scanning       notVulnerable=1 vulnerable=1
root@drews-dev-02 [development] ~ $ /tmp/log4j-jndi-remover fix /srv/some-app
   • ⏳ Collecting jar files from directory parent_path=/srv/some-app
   • 🎯 Found possible targets  found=2 parent=/srv/some-app
   • 🤙 No JNDI found           target=/srv/some-app/jruby_cache_backend.jar
   • 💾 Backup created          backup=/root/.log4shell-fix-backups/logstash-input-tcp-6.2.3.jar-2021-12-16-09-23-49.934738544 target=/srv/some-app/logstash-input-tcp-6.2.3.jar
   • 👍 Removed Jndi Lookup class target=/srv/some-app/logstash-input-tcp-6.2.3.jar
   • ❤ ️Thanks for keeping Duke safe!
root@drews-dev-02 [development] ~ $ /tmp/log4j-jndi-remover scan /srv/some-app
   • ⏳ Collecting jar files from directory parent_path=/srv/some-app
   • 🎯 Found possible targets  found=2 parent=/srv/some-app
   • 🤩 No JNDI class path found target=/srv/some-app/jruby_cache_backend.jar vulnerable=false
   • 🤩 No JNDI class path found target=/srv/some-app/logstash-input-tcp-6.2.3.jar vulnerable=false
   • 🤙 Finished scanning       notVulnerable=2 vulnerable=0
root@drews-dev-02 [development] ~ $
```

## Usage Tips

Use this tool in powershell scripts by converting the output to a powershell object

```powershell
.\log4j-jndi-remover.exe scan 'C:/Program Files/' -f json | ConvertFrom-Json | fl
```
