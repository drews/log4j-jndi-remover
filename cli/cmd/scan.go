package cmd

import (
	"sync"
	"sync/atomic"

	"github.com/apex/log"

	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/drews/log4j-jndi-remover/pkg/utils"
	"gitlab.oit.duke.edu/drews/log4j-jndi-remover/pkg/zip"
)

// scanCmd represents the scan command
var scanCmd = &cobra.Command{
	Use:   "scan",
	Short: "Scan a jar file for the Jndi Lookup class",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		// Determine the scanner
		// var scanner *zip.Scanner
		scanner := zip.NewScanner(ZipScannerType)

		targets, err := utils.CollectTargets(args[0])
		if err != nil {
			log.WithFields(log.Fields{
				"target": args[0],
			}).WithError(err).Fatal("Could not collect targets")
		}

		log.Debugf("Found targets: %v", targets)

		// Only scan 10 at a time
		maxGoroutines := Concurrency
		guard := make(chan struct{}, maxGoroutines)

		targetsLen := len(targets)
		log.WithFields(log.Fields{
			"parent": args[0],
			"found":  targetsLen,
		}).Info("🎯 Found possible targets")

		var vulnCount, notVulnCount uint64

		var wg sync.WaitGroup
		wg.Add(targetsLen)
		for _, target := range targets {
			guard <- struct{}{}
			go func(target string) {
				defer wg.Done()
				log.WithFields(log.Fields{
					"target": target,
				}).Debug("Checking target")
				hasJndi, err := scanner.Scan(target)
				if err != nil {
					log.WithFields(log.Fields{
						"target": target,
					}).WithError(err).Warn("🤒 Could not scan jar file")
				}
				if hasJndi {

					version, err := scanner.ScanVersion(target)
					var vulnerable bool
					if err != nil {
						log.WithFields(log.Fields{
							"target": target,
						}).WithError(err).Warn("😥 Version check failed, treating this as vulnerable")
						vulnerable = true
					} else {
						vulnerable = utils.IsVersionVulnerable(version, GlobalMinVersion, GlobalMaxVersion)
					}
					var versionS string
					if version != nil {
						versionS = version.String()
					} else {
						versionS = "unknown"
					}

					if vulnerable {
						log.WithFields(log.Fields{
							"target":     target,
							"vulnerable": vulnerable,
							"version":    versionS,
						}).Warn("🤢 Found JNDI class path with a possibly vulnerable version")
						atomic.AddUint64(&vulnCount, 1)
					} else {
						log.WithFields(log.Fields{
							"target":     target,
							"vulnerable": vulnerable,
							"version":    versionS,
						}).Info("😅 Found JNDI class path, but version is not vulnerable")
						atomic.AddUint64(&notVulnCount, 1)
					}

				} else {
					log.WithFields(log.Fields{
						"target":     target,
						"vulnerable": false,
					}).Info("🤩 No JNDI class path found")
					atomic.AddUint64(&notVulnCount, 1)
				}
				<-guard
			}(target)
		}
		wg.Wait()
		log.WithFields(log.Fields{
			"vulnerable":    vulnCount,
			"notVulnerable": notVulnCount,
		}).Info("🤙 Finished scanning")
	},
}

func init() {
	rootCmd.AddCommand(scanCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// scanCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// scanCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
