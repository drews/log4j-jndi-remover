/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"os"

	"github.com/apex/log"
	"github.com/apex/log/handlers/cli"
	"github.com/apex/log/handlers/json"
	"github.com/apex/log/handlers/logfmt"
	"github.com/spf13/cobra"

	hversion "github.com/hashicorp/go-version"
	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
)

var (
	cfgFile                            string
	Verbose                            bool
	Quiet                              bool
	Concurrency                        int
	ZipScannerType                     string
	LogFormat                          string
	GlobalMinVersion, GlobalMaxVersion *hversion.Version
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "log4j-jndi-remover",
	Short: "A brief description of your application",
	Long: `A longer description that spans multiple lines and likely contains
examples and usage of using your application. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) { },
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		switch LogFormat {
		case "json":
			log.SetHandler(json.New(os.Stdout))
		case "text":
			log.SetHandler(cli.Default)
		case "logfmt":
			log.SetHandler(logfmt.Default)
		default:
			log.WithField("format", LogFormat).Fatal("Invalid log format")
		}

		if Verbose {
			log.SetLevel(log.DebugLevel)
			log.Debug("Debugging enabled")
		} else if Quiet {
			log.SetLevel(log.WarnLevel)
		}
		min, err := cmd.Flags().GetString("min-version")
		CheckErr(err)
		max, err := cmd.Flags().GetString("max-version")
		CheckErr(err)

		GlobalMinVersion, err = hversion.NewVersion(min)
		CheckErr(err)
		GlobalMaxVersion, err = hversion.NewVersion(max)
		CheckErr(err)

		// Check Versions
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.cli.yaml)")
	rootCmd.PersistentFlags().BoolVarP(&Verbose, "verbose", "v", false, "Enable verbose output")
	rootCmd.PersistentFlags().BoolVarP(&Quiet, "quiet", "q", false, "Only print warning and above")
	rootCmd.PersistentFlags().StringVarP(&ZipScannerType, "zip-scanner", "z", "auto", "Which zip scanner to use: auto/internal/external")
	rootCmd.PersistentFlags().StringVarP(&LogFormat, "format", "f", "text", "Log format (text, json, logfmt)")
	rootCmd.PersistentFlags().IntVarP(&Concurrency, "concurrency", "c", 10, "Number of concurrent workers, where applicable")

	rootCmd.PersistentFlags().String("min-version", "2.0.0", "Minimum version to of log4j that's vulnerable")
	rootCmd.PersistentFlags().String("max-version", "2.16.0", "Maximum version to of log4j that's vulnerable")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".cli" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".cli")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Fprintln(os.Stderr, "Using config file:", viper.ConfigFileUsed())
	}
}

func CheckErr(err error) {
	if err != nil {
		log.WithError(err).Fatal("Error")
	}
}
