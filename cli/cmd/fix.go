/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"sync"

	"github.com/apex/log"

	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/drews/log4j-jndi-remover/pkg/utils"
	"gitlab.oit.duke.edu/drews/log4j-jndi-remover/pkg/zip"
)

// fixCmd represents the fix command
var fixCmd = &cobra.Command{
	Use:   "fix",
	Short: "Fix a jar file for the Jndi Lookup class",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		skipBackup, err := cmd.Flags().GetBool("skip-backup")
		CheckErr(err)
		scanner := zip.NewScanner(ZipScannerType)

		targets, err := utils.CollectTargets(args[0])
		CheckErr(err)

		log.Debugf("Found targets: %v", targets)

		// Only scan 10 at a time
		maxGoroutines := Concurrency
		guard := make(chan struct{}, maxGoroutines)

		targetsLen := len(targets)
		log.WithFields(log.Fields{
			"parent": args[0],
			"found":  targetsLen,
		}).Info("🎯 Found possible targets")

		var wg sync.WaitGroup
		wg.Add(targetsLen)
		for _, target := range targets {
			guard <- struct{}{}
			go func(target string) {
				defer wg.Done()
				hasJndi, err := scanner.Scan(target)
				if err != nil {
					log.WithFields(log.Fields{
						"target": target,
					}).WithError(err).Warn("🤒 Could not scan jar file")
					return
				}
				cobra.CheckErr(err)
				if !hasJndi {
					log.WithFields(log.Fields{
						"target": target,
					}).Info("🤙 No JNDI found")
				} else {

					version, err := scanner.ScanVersion(target)
					var vulnerable bool
					if err != nil {
						log.WithFields(log.Fields{
							"target": target,
						}).WithError(err).Warn("😥 Version check failed, treating this as vulnerable")
						vulnerable = true
					} else {
						vulnerable = utils.IsVersionVulnerable(version, GlobalMinVersion, GlobalMaxVersion)
					}

					var versionS string
					if version != nil {
						versionS = version.String()
					} else {
						versionS = "unknown"
					}

					if vulnerable {
						err = scanner.Fix(target, !skipBackup)
						cobra.CheckErr(err)
						log.WithFields(log.Fields{
							"target":  target,
							"version": versionS,
						}).Warn("👍 Removed Jndi Lookup class")
					} else {
						log.WithFields(log.Fields{
							"target":  target,
							"version": versionS,
						}).Info("😅 Found Jndi Lookup class, but not a vulnerable version")
					}

				}
				<-guard
			}(target)
		}
		wg.Wait()
		log.Info("❤ ️Thanks for keeping Duke safe!")
	},
}

func init() {
	rootCmd.AddCommand(fixCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// fixCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	fixCmd.PersistentFlags().BoolP("skip-backup", "s", false, "Skip backups")
}
